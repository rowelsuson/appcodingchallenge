//
//  UserDefaultKey.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/9/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation

enum UserDefaultKey: String {
    case kPreviousDate = "kPreviousDate"
}
