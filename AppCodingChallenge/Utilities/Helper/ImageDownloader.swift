//
//  ImageDownloader.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/8/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import UIKit
import SDWebImage

class ImageDownloader {
    class func loadImageWithURL(url strURL:String,
                                placeholderImage:UIImage?,
                                placeholder: ((_ image: UIImage?) -> Void)?,
                                cachedImage: ((_ image: UIImage?) -> Void)?,
                                completion:@escaping (_ image: UIImage, _ forKey: String) -> Void) {
        let downloader = SDWebImageDownloader.shared
        let url = URL(string: strURL)
        
        let cachedImg = SDImageCache.shared.imageFromMemoryCache(forKey: strURL)
        if cachedImg != nil {
            if let block = cachedImage { block(cachedImg) }
        } else if strURL.isEmpty, let block = placeholder {
            block(placeholderImage)
        } else {
            if let block = placeholder { block(placeholderImage) }
            downloader.downloadImage(with: url, options:.useNSURLCache, progress: nil) { (image, data, error, status) in
                if error == nil {
                    // Cache Image
                    SDImageCache.shared.store(image, forKey: strURL, toDisk: false)
                    // Move to main queue
                    DispatchQueue.main.async(execute: {
                        completion(image!, strURL)
                    })
                }
            }
        }
    }
    
}
