//
//  ErrorType.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/6/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum HTTPErrorType: Int, Error {
    case clientError  = 400
    case invalidToken = 401
    case forbidden    = 403
    case notFound     = 404
}

enum ServerErrorType: Int, Error {
    case internalServerError = 500
    case serviceUnavailable  = 503
}

enum HTTPResponseError: Error {
    case nilSignal
    case noInternet
    case httpError(HTTPErrorType)
    case serverError(ServerErrorType)
    case unknown(String)
    case responseError
    
    static func errorType(withCode code: Int) -> HTTPResponseError {
        if 400..<500 ~= code { // HTTP Error
            return .httpError(HTTPErrorType(rawValue: code) ?? .clientError)
        } else if 500..<600 ~= code { // Server Error
            return .serverError(ServerErrorType(rawValue: code) ?? .internalServerError)
        } else {
            if code == -1009 {
                return .noInternet
            }
            
            return .unknown("Unknown Error")
        }
    }
}

struct APIError: Error {
    var status: HTTPResponseError
    var message: String?
    
    init(status: HTTPResponseError) {
        self.status   = status
    }
    
    init<Value>(dataResponse: DataResponse<Value>) {
        status = HTTPResponseError.errorType(withCode: dataResponse.response?.statusCode ?? 0)
        
        if let responseData = dataResponse.data {
            message = "Unknown Error"
            
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: responseData, options: [])
                
                if let message = (jsonResponse as? [String: String])?["message"] {
                    self.message = message
                }
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
    }
}
