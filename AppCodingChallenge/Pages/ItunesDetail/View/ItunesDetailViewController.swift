//
//  ItunesDetailViewController.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/8/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import UIKit

class ItunesDetailViewController: UIViewController {
    
    var viewModel: ItunesDetailViewModel

    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Initalize Controller
    init(viewModel: ItunesDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: ItunesDetailViewController.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.registerReusableCell(ItemDetailInfoCell.self)
        self.tableView.registerReusableCell(ItemDetailDescriptionCell.self)
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 100
        self.tableView.contentInsetAdjustmentBehavior = .automatic
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}

// MARK: - UITableViewDataSource
extension ItunesDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(indexPath) as ItemDetailInfoCell
            cell.configureCell(viewModel: viewModel)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(indexPath) as ItemDetailDescriptionCell
            cell.configureCell(viewModel: viewModel)
            return cell
        }
    }
}

// MARK: - UITableViewDelegate
extension ItunesDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
