//
//  ItemDetailInfoCell.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/8/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class ItemDetailInfoCell: UITableViewCell, NibCellReusable {
    
    static var height: CGFloat {return 245}
    
    // MARK: IBOutlets
    @IBOutlet weak var imageArtwork: UIImageView!
    @IBOutlet weak var imageActivity: UIActivityIndicatorView!
    @IBOutlet weak var labelTrackname: UILabel!
    @IBOutlet weak var labelArtist: UILabel!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelHDPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /// Thiis will be called to configure the data of the cell
    func configureCell(viewModel: ItunesDetailViewModel) {
        labelTrackname.reactive.text <~ viewModel.trackName
        labelArtist.reactive.text <~ viewModel.artist
        labelGenre.reactive.text <~ viewModel.genre
        labelPrice.reactive.text <~ viewModel.price
        labelHDPrice.reactive.text <~ viewModel.priceHD
        
        downloadArtwork(url: viewModel.artworkURL.value)
    }
    
    /// To download the image url
    fileprivate func downloadArtwork(url: String) {
        imageActivity.startAnimating()
        
        let placeholderImage = #imageLiteral(resourceName: "imageNotFound")
        ImageDownloader.loadImageWithURL(url: url, placeholderImage: placeholderImage,
                                         placeholder: ({ [weak self] image in
                                            self?.setImage(image: image)
                                         }), cachedImage: ({ [weak self] image in
                                            self?.setImage(image: image)
                                         })) { [weak self](image, cachedKey) in
                                            self?.setImage(image: image)
        }
        
    }
    
    /// To set the image into ImageView
    fileprivate func setImage(image: UIImage?) {
        if let img = image {
            self.imageArtwork.image = img
            self.setNeedsLayout()
            self.imageActivity.stopAnimating()
        }
        
    }
    
}
