//
//  ItemDetailDescriptionCell.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/8/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class ItemDetailDescriptionCell: UITableViewCell, NibCellReusable {
    
    static var height: CGFloat {return 100}
    
    //MARK: IBOutlets
    
    @IBOutlet weak var labelLongDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /// This method will be called to configure the data of the cell
    func configureCell(viewModel: ItunesDetailViewModel) {
        labelLongDescription.reactive.text <~ viewModel.longDescription
    }
    
}
