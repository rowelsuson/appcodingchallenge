//
//  ItunesDetailViewModel.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/8/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ReactiveSwift

class ItunesDetailViewModel {
    
    private var selectedSearchItem: SearchItem!
    
    var trackName = MutableProperty("")
    var artist = MutableProperty("")
    var genre = MutableProperty("")
    var price = MutableProperty("")
    var priceHD = MutableProperty("")
    var artworkURL = MutableProperty("")
    var longDescription = MutableProperty("")
    
    
    init(item: SearchItem) {
        selectedSearchItem = item
        
        trackName.value = selectedSearchItem.trackName ?? ""
        artist.value = selectedSearchItem.artistName ?? ""
        genre.value = selectedSearchItem.primaryGenreName ?? ""
        price.value = "\(selectedSearchItem.currency ?? "") \(selectedSearchItem.trackPrice)"
        priceHD.value = "\(selectedSearchItem.currency ?? "") \(selectedSearchItem.trackHDPrice)"
        artworkURL.value = selectedSearchItem.artworkUrl100 ?? ""
        longDescription.value = selectedSearchItem.longDescription ?? ""
    }
    
    // MARK: - Functions
    
}
