//
//  ItunesMasterViewController.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import UIKit

class ItunesMasterViewController: UIViewController {
    
    
    var viewModel: ItunesMasterViewModel!
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- Header
    fileprivate var headerView: ItunesMasterHeaderView? = ItunesMasterHeaderView.nib()
    
    // MARK: - Initalize Controller
    init(viewModel: ItunesMasterViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: ItunesMasterViewController.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.registerReusableCell(ItemCell.self)
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 100
        self.tableView.contentInsetAdjustmentBehavior = .automatic
        
        addAppDeactivationObserver()
        reactiveBind()
        viewModel.search()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "MOVIES"
    }

}

// MARK:- Binding Reactive Observer
extension ItunesMasterViewController {
    /// Bind the reactive observers from the ViewModel
    func reactiveBind() {
        viewModel.loaderObserver.output.observeValues { [weak self](show) in
            if show {
                self?.showLoader()
            } else {
                self?.hideLoader()
            }
        }
        
        viewModel.reloadObserver.output.observeValues { [weak self](isReload) in
            if isReload {
                self?.updateHeader()
                self?.tableView.reloadData()
            }
        }
        
        viewModel.errorObserver.output.observeValues { [weak self]() in
            self?.showErrorMessage()
        }
        
        viewModel.showDetailObserver.output.observeValues { [weak self](itunesDetailViewController) in
            self?.navigationController?.pushViewController(itunesDetailViewController, animated: true)
        }
    }
}

// MARK:- Loading Alert
extension ItunesMasterViewController {
    /// To show progress loader in forms of UIAlert
    func showLoader() {
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = .medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func hideLoader() {
        dismiss(animated: true) {}
    }
    
    /// Show Error message if in case there is problem connecting the the api service
    func showErrorMessage() {
        let alert = UIAlertController(title: "Error!",
                                      message: "There is a problem loading the data. Please try again!",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self](action) in
            self?.viewModel.search()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - Update data in Header View
extension ItunesMasterViewController {
    /// Method to call to update the viewHeader
    fileprivate func updateHeader() {
        let lastVisitDate = viewModel.getLastVisitedDate()
        headerView?.configureHeader(resultsCount: viewModel.searchItems.count, lastVisitDate: lastVisitDate)
    }
}

// MARK: - UITableViewDataSource
extension ItunesMasterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath) as ItemCell
        
        cell.configureCell(item: viewModel.searchItems[indexPath.row])
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ItunesMasterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(indexPath: indexPath)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return ItunesMasterHeaderView.height
    }
    
}

// MARK: - SetUp For App Deactivation Observer
extension ItunesMasterViewController {
    /// Add notifications observer when the app goes to deactive or went to background
    fileprivate func addAppDeactivationObserver() {
        if #available(iOS 13.0, *) {
            NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive), name: UIScene.willDeactivateNotification, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(appWillActiveAgain), name: UIScene.didActivateNotification, object: nil)
            
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(appWillActiveAgain), name: UIApplication.didBecomeActiveNotification, object: nil)
        }
    }
    
    /// A selector function when deactivation of app has been notified
    @objc func appWillResignActive(_ notification: Notification) {
        viewModel.saveLastVisitDate()
    }
    
    /// A selector function when activate again
    @objc func appWillActiveAgain(_ notification: Notification) {
        self.updateHeader()
    }
}
