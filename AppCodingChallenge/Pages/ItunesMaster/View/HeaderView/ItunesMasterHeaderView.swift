//
//  ItunesMasterHeaderView.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/9/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import UIKit

class ItunesMasterHeaderView: ReusableView {
    
    //MARK: IBOutlets
    @IBOutlet weak var labelResults: UILabel!
    @IBOutlet weak var labelLastVisited: UILabel!
    
    static var height: CGFloat { return 52 }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
       if self.subviews.count == 0 {
            let realView = ItunesMasterHeaderView.nib()
            // pass properties through
            setupRealView(view: realView)
            return realView
        }
        
        return self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /// Configure the data of the Master ViewHeader
    func configureHeader(resultsCount: Int, lastVisitDate: Date?) {
        labelResults.text = "Search results: \(resultsCount)"
        
        if let lastDate = lastVisitDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
            labelLastVisited.text = "Last visit: \(dateFormatter.string(from: lastDate))"
        } else {
            labelLastVisited.text = ""
        }
       
    }

}
