//
//  ItunesMasterViewModel.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ReactiveSwift
import Swinject

class ItunesMasterViewModel {
    var searchService: SearchService!
    var resolver: Resolver!
    
    // MARK: Observer
    var loaderObserver = Signal<Bool, Never>.pipe()
    var errorObserver  = Signal<(), Never>.pipe()
    var reloadObserver = Signal<Bool, Never>.pipe()
    var showDetailObserver = Signal<ItunesDetailViewController, Never>.pipe()
    
    // MARK: Property
    var searchItems = [SearchItem]()
    
    // MARK: - Functions
    
    /// search method of ViewModel use to call the seach functions from the Service
    func search() {
        searchService.search(term: "star", country: "au", media: "movie")
            .on(started: { [weak self] in
                self?.loaderObserver.input.send(value: true)
            }, failed: { [weak self](error) in
                self?.loaderObserver.input.send(value: false)
                self?.errorObserver.input.send(value: ())
            }, value: { [weak self](items) in
                self?.searchItems = items
                self?.loaderObserver.input.send(value: false)
                self?.reloadObserver.input.send(value: true)
            }).start()
    }
    
    /// this method will be called if there any item has been selected in the ViewController
    func didSelect(indexPath: IndexPath) {
        let searchItem = searchItems[indexPath.row]
        let detailViewController = resolver.resolve(ItunesDetailViewController.self, argument:searchItem)!
        showDetailObserver.input.send(value: detailViewController)
    }
    
    
    /// this method will be called to save the current date into UserDefaults
    func saveLastVisitDate() {
        print("Save Date")
        UserDefaults.standard.set(Date(), forKey: UserDefaultKey.kPreviousDate.rawValue)
    }
    
    
    /// thiss method will be called to fetch the last visit date in UserDefaults
    func getLastVisitedDate() -> Date? {
        return UserDefaults.standard.object(forKey: UserDefaultKey.kPreviousDate.rawValue) as? Date
    }
}
