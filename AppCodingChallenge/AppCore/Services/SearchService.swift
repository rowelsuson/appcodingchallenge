//
//  SearchService.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ReactiveSwift


class SearchService {
    var networkSource: ServiceAPISearchProtocol!
    var databaseSource: DataStoreSearchItemProtocol!
}

extension SearchService: iTunesSearchServiceProtocol {
    
    /// To call search items from Database or in web service
    func search(term: String, country: String, media: String) -> SignalProducer<[SearchItemDTO], APIError> {
        return SignalProducer { [unowned self] (observer, _ ) in
            
            // Fetch database of search items if it is exist
            let searchItems = self.databaseSource.fetchSearchItems()
            if searchItems.count > 0 {
                observer.send(value: searchItems.map({$0.toDTO()}))
            }
            
            var params = SearchRequestDTO()
            
            params.term    = term
            params.country = country
            params.media   = media
            
            self.networkSource.search(params).startWithResult { (result) in
                switch result {
                case .success(let response):
                    let searchItems = response.results
                    self.databaseSource.persist(searchItems: searchItems)
                    
                    observer.send(value: searchItems.map({$0.toDTO()}))
                    observer.sendCompleted()
                    break
                case .failure(let error):
                    observer.send(error: error)
                    observer.sendCompleted()
                }
            }
        }
    }
    
    func fetchSearchItems() -> [SearchItemDTO] {
        return self.databaseSource.fetchSearchItems().map({$0.toDTO()})
    }
    
}
