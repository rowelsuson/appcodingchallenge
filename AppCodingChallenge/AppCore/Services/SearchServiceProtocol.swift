//
//  SearchServiceProtocol.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol iTunesSearchServiceProtocol {
    
    /// Search contents in iTunes
    func search(term: String, country: String, media: String)
        -> SignalProducer<[SearchItemDTO], APIError>
    
    
    /// Fetch search items from realm database
    func fetchSearchItems() -> [SearchItemDTO]
    
    
    
}
