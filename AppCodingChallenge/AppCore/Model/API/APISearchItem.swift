//
//  APISearchItem.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ObjectMapper

class ApiSearchItem: Mappable, SearchItem {
    var trackId: Int = 0
    var trackName: String?
    var artistName: String?
    var artworkUrl100: String?
    var trackPrice: Double = 0.00
    var trackHDPrice: Double = 0.00
    var currency: String?
    var primaryGenreName: String?
    var longDescription: String?
    
    
    required init() { }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        trackId             <- map["trackId"]
        trackName           <- map["trackName"]
        artistName          <- map["artistName"]
        artworkUrl100       <- map["artworkUrl100"]
        trackPrice          <- map["trackPrice"]
        trackHDPrice        <- map["trackHdPrice"]
        currency            <- map["currency"]
        primaryGenreName    <- map["primaryGenreName"]
        longDescription     <- map["longDescription"]
        
    }
}
