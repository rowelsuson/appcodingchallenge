//
//  File.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ObjectMapper

class ApiSearchResponse: Mappable, SearchResponse {
    var resultCount: Int = 0
    
    fileprivate var _results: [ApiSearchItem]?
    var results: [SearchItem] {
        get {
            return _results?.map({ $0.toApiModel() }) ?? []
        } set {
            _results = newValue.map({ $0.toApiModel() })
        }
    }
    
    required init() { }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        resultCount         <- map["resultCount"]
        _results            <- map["results"]
    }
}
