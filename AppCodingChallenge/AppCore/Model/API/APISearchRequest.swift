//
//  APISearchRequest.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ObjectMapper

class ApiSearchRequest: Mappable, SearchRequest {
    var term: String    = ""
    var country: String = ""
    var media: String   = ""
    
    required init() { }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        term     <- map["term"]
        country  <- map["country"]
        media    <- map["media"]
    }
}
