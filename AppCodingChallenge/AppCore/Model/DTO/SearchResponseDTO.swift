//
//  SearchResponseDTO.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation

struct SearchResponseDTO: SearchResponse {
    var resultCount: Int = 0
    var results = [SearchItem]()
}

struct SearchItemDTO: SearchItem {
    var trackId: Int = 0
    var trackName: String?
    var artistName: String?
    var artworkUrl100: String?
    var trackPrice: Double = 0.00
    var trackHDPrice: Double = 0.00
    var currency: String?
    var primaryGenreName: String?
    var longDescription: String?
    
}
