//
//  SearchRequestDTO.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation

struct SearchRequestDTO: SearchRequest {
    var term: String    = ""
    var country: String = ""
    var media: String   = ""
}
