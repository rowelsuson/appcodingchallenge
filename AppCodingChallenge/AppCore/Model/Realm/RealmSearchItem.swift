//
//  RealmSearchResponse.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import RealmSwift

class RealmSearchItem: Object, SearchItem {
    
    @objc dynamic var trackId: Int = 0
    @objc dynamic var trackName: String?
    @objc dynamic var artistName: String?
    @objc dynamic var artworkUrl100: String?
    @objc dynamic var trackPrice: Double = 0
    @objc dynamic var trackHDPrice: Double = 0
    @objc dynamic var currency: String?
    @objc dynamic var primaryGenreName: String?
    @objc dynamic var longDescription: String?
    
    override class func primaryKey() -> String? {
        return "trackId"
    }
}
