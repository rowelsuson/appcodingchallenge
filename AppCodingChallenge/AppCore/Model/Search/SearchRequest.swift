//
//  SearchRequest.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation

protocol SearchRequest {
    var term: String { get set }
    var country: String { get set }
    var media: String { get set }
    
    init()
    init(copy obj: SearchRequest)
}

extension SearchRequest {
    init(copy obj: SearchRequest) {
        self.init()
        
        term    = obj.term
        country = obj.country
        media   = obj.media
    }
    
    func toDTO() -> SearchRequestDTO {
        return SearchRequestDTO(copy: self)
    }
    
    func toApiModel() -> ApiSearchRequest {
        return ApiSearchRequest(copy: self)
    }
}
