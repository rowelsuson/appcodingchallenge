//
//  SearchResponse.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation

protocol SearchResponse {
    var resultCount: Int    { get set }
    var results: [SearchItem]  { get set }
    
    init()
    init(copy obj: SearchResponse)
}

extension SearchResponse {
    
    init(copy obj: SearchResponse) {
        self.init()
        
        resultCount = obj.resultCount
        results     = obj.results
    }
    
    func toDTO() -> SearchResponseDTO {
        return SearchResponseDTO(copy: self)
    }
    
    
    func toApiModel() -> ApiSearchResponse {
        return ApiSearchResponse(copy: self)
    }
}


