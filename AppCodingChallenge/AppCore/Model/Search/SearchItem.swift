//
//  SearchItem.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation

protocol SearchItem {
    var trackId: Int                { get set }
    var trackName: String?          { get set }
    var artistName: String?         { get set }
    var artworkUrl100: String?      { get set }
    var trackPrice: Double          { get set }
    var trackHDPrice: Double        { get set }
    var currency: String?           { get set }
    var primaryGenreName: String?   { get set }
    var longDescription: String?    { get set }
    
    
    init()
    init(copy obj: SearchItem)
}

extension SearchItem {
    
    init(copy obj: SearchItem) {
        self.init()
        
        trackId             = obj.trackId
        trackName           = obj.trackName
        artistName          = obj.artistName
        artworkUrl100       = obj.artworkUrl100
        trackPrice          = obj.trackPrice
        trackHDPrice        = obj.trackHDPrice
        currency            = obj.currency
        primaryGenreName    = obj.primaryGenreName
        longDescription     = obj.longDescription
        
    }
    
    func toDTO() -> SearchItemDTO {
        return SearchItemDTO(copy: self)
    }
    
    func toRealm() -> RealmSearchItem {
        return RealmSearchItem(copy: self)
    }
    
    func toApiModel() -> ApiSearchItem {
        return ApiSearchItem(copy: self)
    }
}
