//
//  AppConfigurable.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/6/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ObjectMapper

enum BaseURLType {
    case `default`
    
    var description: String {
        switch self {
        case .`default`:
            return "baseURL"
        }
    }
}

protocol ApplicationConfigurable {
    func baseURL(type: BaseURLType) -> String
    
    func defaultMethod() -> String
    
    func uri() -> Dictionary<String, ApiMethodInfo?>
 }
