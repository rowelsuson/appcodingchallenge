//
//  URI.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/6/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation

enum ApiMethod: String {
    case search = "search"
    
    var baseURLType: BaseURLType {
        switch self {
        default:
            return BaseURLType.default
        }
    }
}
