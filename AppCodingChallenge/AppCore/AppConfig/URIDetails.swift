//
//  URIDetails.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/6/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

protocol ApiMethodInfo: Mappable  {
    var httpHeaders: [String: String]? { get }
    var urlQuery: String? { get }
    
    var method: HTTPMethod { get }
    var path: String { get }
    
    mutating func setURIPath(_ newUri: String)
}

struct URIDetails: ApiMethodInfo {
    internal var httpHeaders: [String : String]?
    internal var urlQuery: String?
    
    fileprivate var methodREST: String?
    fileprivate var uriPath: String?
    
    var method: HTTPMethod {
        return HTTPMethod(rawValue: self.methodREST ?? "GET")!
    }
    
    var path: String {
        return self.uriPath ?? ""
    }
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        httpHeaders     <- map["httpHeaders"]
        urlQuery        <- map["urlQuery"]
        methodREST      <- map["method"]
        uriPath         <- map["path"]
    }
    
    mutating func setURIPath(_ newUri: String) {
        self.uriPath = newUri
    }
}
