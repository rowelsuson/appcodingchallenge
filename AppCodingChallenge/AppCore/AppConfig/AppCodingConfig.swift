//
//  AppCodingConfig.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/6/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ObjectMapper

struct AppCodingConfig: ApplicationConfigurable {
    fileprivate let rootPlist: [String: Any]?
    
    
    fileprivate var backend: [String: Any]? {
        return self.rootPlist?["Backend"] as? [String: Any]
    }
    
    fileprivate var appState: [String: Any]? {
        return self.rootPlist?["AppState"] as? [String: Any]
    }
    
    
    init() {
        let bundle = Bundle.main
        
        let path  = bundle.path(forResource: "Config", ofType: "plist") ?? ""
        rootPlist = NSDictionary(contentsOfFile: path) as? [String: Any]
    }
    
    // MARK: Protocol Implementation
    
    func baseURL(type: BaseURLType) -> String {
        guard let url:String = backend?[type.description] as? String
            else { return "" }
        return  url
    }
    
    func defaultMethod() -> String {
        guard let method = backend?["DefaultMethod"] as? String else { return "" }
        return method
    }
    
    func uri() -> Dictionary<String, ApiMethodInfo?> {
        guard let tmpUriList = backend?["URI"] as? Dictionary<String, AnyObject> else { return [:] }
        
        var uriList: Dictionary<String, ApiMethodInfo?> = [:]
        var newValue: ApiMethodInfo?
        for (key, value) in tmpUriList {
            if let value = value as? [String: Any] {
                newValue = Mapper<URIDetails>().map(JSON: value)
                uriList[key] = newValue
            }
        }
        
        return  uriList
    }
    
    
}
