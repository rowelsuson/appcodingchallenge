//
//  ServiceAPI.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ReactiveSwift
import ObjectMapper

final class ServiceApi {
    let networkService: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func signalProducer<Value: Mappable>(baseURL: BaseURLType,
                                         uri: ApiMethodInfo,
                                         params:[String : Any]? = nil)
        -> SignalProducer<Value, APIError> {
            return SignalProducer { [unowned self] (observer, _) in
                self.networkService
                    .request(baseURL,uri: uri, params: params,
                             responseHandler: { (response: ResponseData<Value>) in
                                switch response.result {
                                case .success(let value):
                                    observer.send(value: value)
                                case .failure(let error):
                                    observer.send(error: error as! APIError)
                                }
                    })
            }
    }
}

extension ServiceApi {
    
    func errorNilSignal<Value>() -> SignalProducer<Value, APIError> {
        return SignalProducer { (observer, _) in
            observer.send(error: APIError(status: .nilSignal) )
            observer.sendCompleted()
        }
    }
}
