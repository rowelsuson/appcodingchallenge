//
//  ServiceAPISearch.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/7/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ReactiveSwift
import ObjectMapper

protocol ServiceAPISearchProtocol {
    func search<Value: ApiSearchResponse>(_ requestParams: SearchRequest)
        -> SignalProducer<Value, APIError>
}

extension ServiceApi: ServiceAPISearchProtocol {
    
    func search<Value: ApiSearchResponse>(_ requestParams: SearchRequest)
        -> SignalProducer<Value, APIError>
    {
        let method = ApiMethod.search
        guard let uriDetails = self.networkService.getURIDetails(apiMethod: method)
            else { return errorNilSignal() }
        
        return signalProducer(baseURL: method.baseURLType, uri: uriDetails, params: requestParams.toApiModel().toJSON())
    }
}
