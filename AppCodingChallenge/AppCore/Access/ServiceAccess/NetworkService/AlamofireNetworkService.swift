//
//  AlamofireNetworkService.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/6/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

final class AlamofireNetworkService: NetworkServiceProtocol {
    
    fileprivate var backgroundQueue: DispatchQueue {
        return DispatchQueue(label: "com.rowel.networklog", attributes: DispatchQueue.Attributes.concurrent)
    }
    
    var appConfig: ApplicationConfigurable
    
    init(appConfig: ApplicationConfigurable) {
        self.appConfig = appConfig
    }
    
    func request<Value: Mappable>(_ baseURL: BaseURLType,
                                  uri URIDetails:ApiMethodInfo,
                                  params:[String : Any]?,
                                  responseHandler: @escaping(_ response: ResponseData<Value>) -> ()) {
        let URLRequest = paramEncoding(type: baseURL, uri:URIDetails, params:params)
        
            
        Alamofire.request(URLRequest)
            .responseObject(completionHandler: { (response: DataResponse<Value>) in
                switch response.result {
                case .success(let value):
                    if self.validate(response: response.response) {
                        responseHandler(ResponseData(result: .success(value)))
                        break
                    }
                    
                    fallthrough
                case .failure(_):
                    let error = APIError(dataResponse: response)
                    
                    responseHandler(ResponseData(result: .failure(error)))
                }
            }).responseJSON { [unowned self](response) in
                self.logResponse(response)
        }
    }
    
}

// MARK: Private Functions
extension AlamofireNetworkService {
    
    fileprivate func paramEncoding(type: BaseURLType, uri URIDetails: ApiMethodInfo, params:[String : Any]?) -> URLRequest {
        let URL = Foundation.URL(string: appConfig.baseURL(type: type))!
        var request = urlRequest(query: URIDetails.urlQuery)(URLRequest(url: URL.appendingPathComponent(URIDetails.path)))
        
        request.httpMethod = URIDetails.method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.timeoutInterval = 120
        
        logRequest(paramsModel: params, request: request.url?.absoluteString)
        
        if let headers = URIDetails.httpHeaders {
            headers.forEach({ (key, value) in
                request.setValue(value, forHTTPHeaderField: key)
            })
        }
        
        let finalRequest: URLRequest
        if URIDetails.method == .get  {
            finalRequest = try! URLEncoding().encode(request, with: params)
        } else {
            finalRequest = try! JSONEncoding().encode(request, with: params)
        }
        
        return finalRequest
    }
    
    fileprivate func urlRequest(query urlQuery: String?) -> (_ request: URLRequest) -> URLRequest {
        return { request in
            guard let query = urlQuery else {
                return request
            }
            
            let urlComponent = NSURLComponents(string: (request.url?.absoluteString)!)!
            urlComponent.query = query
            return URLRequest(url: urlComponent.url!)
        }
    }
    
    fileprivate func validate(response: HTTPURLResponse?) -> Bool {
        return (response?.statusCode ?? 400) == 200
    }
    
    // MARK:- Log Response
    fileprivate func logResponse(_ response: DataResponse<Any>) {
        self.backgroundQueue.async {
            print(response.request?.url?.absoluteString ?? "url not found")
            switch response.result {
            case .success(let value):
                print(value)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK:- Log Request
    fileprivate func logRequest(paramsModel model: [String: Any]?, request: String?) {
        guard let _model = model,
            let _request = request else { return }
        
        print("\nRequest: \(_request)")
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: _model, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            print(String(data: jsonData, encoding: .utf8) ?? "Error - Could not convert to JSON string")
        } catch {
            print(error.localizedDescription)
        }
    }
}
