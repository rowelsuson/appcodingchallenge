//
//  NetworkServiceProtocol.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/6/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import ObjectMapper

enum Response<Value> {
    case success(Value)
    case failure(Error)
}

struct ResponseData<Value> {
    var result: Response<Value>
}

protocol NetworkServiceProtocol {
    var appConfig: ApplicationConfigurable { get set }
    
    func request<Value: Mappable>(_ baseURL: BaseURLType,
                                  uri URIDetails:ApiMethodInfo,
                                  params:[String : Any]?,
                                  responseHandler: @escaping(_ response: ResponseData<Value>) -> ())
}

extension NetworkServiceProtocol {
    // MARK:- Get URI Details from URI Method Type
    func getURIDetails(apiMethod method: ApiMethod) -> ApiMethodInfo? {
        guard case let uriDetails?? = self.appConfig.uri()[method.rawValue] else { return nil }
        
        return uriDetails
    }
    
    // MARK:- Log Request
    func logRequest<T: Mappable>(paramsModel model: T, request: String) {
        print("\n\(model.toJSONString() ?? "\(request): No Request")")
    }
}
