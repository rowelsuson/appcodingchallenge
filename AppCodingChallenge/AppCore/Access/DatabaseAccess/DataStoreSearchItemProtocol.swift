//
//  DataStoreSearchItemProtocol.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/8/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import RealmSwift

protocol DataStoreSearchItemProtocol {
    
    func persist(searchItems: [SearchItem])
    
    func fetchSearchItems() -> [SearchItem]
    
}
