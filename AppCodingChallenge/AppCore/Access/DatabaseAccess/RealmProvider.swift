//
//  RealmProvider.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/8/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import RealmSwift

class RealmProvider: DatabaseCRUDProtocol {
    
}

extension RealmProvider: DataStoreSearchItemProtocol {
    
    func persist(searchItems: [SearchItem]) {
        searchItems.forEach { (item) in
            update(item.toRealm())
        }
    }
    
    func fetchSearchItems() -> [SearchItem] {
        guard let results = self.getAllEntities(ofType: RealmSearchItem.self)?.map({$0}) else { return []}
        return Array(results)
    }

}

