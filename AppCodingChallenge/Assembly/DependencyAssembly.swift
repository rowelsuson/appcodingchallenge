//
//  DependencyAssembly.swift
//  AppCodingChallenge
//
//  Created by Rowel Suson on 3/8/20.
//  Copyright © 2020 Rowel Suson. All rights reserved.
//

import Foundation
import Swinject

class DependencyAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(ApplicationConfigurable.self) { _ in
            return AppCodingConfig()
        }.inObjectScope(.container)
        
        container.register(NetworkServiceProtocol.self) { resolver in
            return AlamofireNetworkService(appConfig: resolver.resolve(ApplicationConfigurable.self)!)
        }.inObjectScope(.weak)
        
        container.register(ServiceApi.self) { resolver in
            return ServiceApi(networkService: resolver.resolve(NetworkServiceProtocol.self)!)
        }.inObjectScope(.weak)
        
        container.register(RealmProvider.self) { _ in
            return RealmProvider()
        }.inObjectScope(.weak)
        
        container.register(SearchService.self) { resolver in
            let service = SearchService()
            service.networkSource  = resolver.resolve(ServiceApi.self)!
            service.databaseSource = resolver.resolve(RealmProvider.self)!
            return service
        }.inObjectScope(.weak)
        
        container.register(ItunesMasterViewController.self) { (resolver) in
            let viewController = ItunesMasterViewController(viewModel: resolver.resolve(ItunesMasterViewModel.self)!)
            return viewController
        }
        
        container.register(ItunesMasterViewModel.self) { resolver in
            let viewModel = ItunesMasterViewModel()
            viewModel.searchService  = resolver.resolve(SearchService.self)!
            viewModel.resolver = resolver
            return viewModel
        }
        
        container.register(ItunesDetailViewController.self) { (resolver, searchItem: SearchItem) in
            let viewController = ItunesDetailViewController(viewModel: resolver.resolve(ItunesDetailViewModel.self, argument: searchItem)!)
            return viewController
        }
        
        container.register(ItunesDetailViewModel.self) { (resolver, searchItem: SearchItem) in
            let viewModel = ItunesDetailViewModel(item: searchItem)
            return viewModel
        }
        
        
    }
}
