
##App Coding Challenge
- an Master Detail application that display list of movies item from Itunes search and then it will show the detail of each selected item.
- this is part of my coding challenge exam

#Architecture
    MVVM - (Model View - View Model) separation of concern architecture

##Libraries
#HTTP Request library
   Alamofire - efficient library for working HTTP networking in Swift IOS

#Dependency Injection
    Swinject - a design pattern that implements Inversion of Control (IoC) for resolving dependencies
    
#Database
    Realm - fast and easy to integrate database manipulation
    
#Functional Reactive
    ReactiveSwift - binding mechanism for MVVM Arrchitecture

#Other Libraries
    SDWebImage - I used this library because it has async image downloader and with cache support.
    
#Folders
Helper - contains Errortype file that has differentation enumeration items which can be shared and access to the entire application.
Assembly - contains DependencyAssembly that holds different assembly intances
AppCore - this folder contain different core of the apps
        AppConfig - contains application configurations that can be found in Config pLisst.
        Access - contains access point for Service(api) and Database
        Model - contains different models (Search model, DTO, Database model, API model)
        Service - contains business functions and connections between Access
Resources - contains the Conflig pList
Pages - this contains the different page features of the application, example for this app are the ItunesMaster and ItunesDetail
        View - contains different views that includes cell, header and view controllers.
        ViewModel - contains the viewModel of each view controller
Utilities - contains Helper files and components extensions.

#Other Information
    Im using XCode 11.3.1 with Swift 5

    
    
    
  
    
    
    
    
    

